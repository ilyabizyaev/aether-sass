var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('build/')
    // public path used by the web server to access the output path
    .setPublicPath('/aether-devel')
    // only needed for CDN's or sub-directory deploy
    .setManifestKeyPrefix('aether-devel/')

    // shared between all websites
    .addEntry('bootstrap', './js/bootstrap.js')

    // website specific code
    .addEntry('drupal', './js/drupal.js')
    .addEntry('charjs', './js/chart.js')
    .addEntry('piwikbanner', './js/piwikbanner.js')

    .addStyleEntry('aether-mediawiki', './css/aether-mediawiki.scss')
    .addStyleEntry('aether-kde-org', './css/aether-kde-org.scss')

    // kde.org subpage
    .addEntry('announcement', './js/announcement.js')
    .addStyleEntry('applications', './css/applications.scss')
    .addStyleEntry('products/kirigami', './css/kirigami.scss')
    .addStyleEntry('products', './css/products.scss')
    .addStyleEntry('plasma-desktop', './css/plasma-desktop.scss')
    .addStyleEntry('kde-org/clipart', './css/clipart.scss')
    .addStyleEntry('kde-org/frameworks', './css/frameworks.scss')
    .addStyleEntry('kde-org/releaseAnnouncement', './css/kde-org/releaseAnnouncement.scss')

    // font only
    .addStyleEntry('breeze-font', './css/style.scss')

    // .enableSingleRuntimeChunk()
    .disableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())

    .configureFilenames({
        images: 'images/[name].[ext]',
        fonts: 'fonts/[name].[ext]'
    })

    .autoProvidejQuery()

    // uncomment if you use Sass/SCSS files
    .enableSassLoader()
;

if (process.env.VERSIONING === "true") {
    Encore.enableVersioning()
        .setOutputPath('build/version')
        .setPublicPath('/aether-devel/version')
        .setManifestKeyPrefix('aether-devel/version/')

        .configureFilenames({
            images: 'images/[name].[hash:8].[ext]',
            fonts: 'fonts/[name].[hash:8].[ext]'
        });
}


if (process.env.LOCAL === "true") {
    Encore.setOutputPath('build/version')
        .setPublicPath('/build/version')
        .setManifestKeyPrefix('build/version/')
        .configureFilenames({
            images: 'images/[name].[hash:8].[ext]',
            fonts: 'fonts/[name].[hash:8].[ext]'
        });

    if (process.env.VERSIONING === "true") {
        Encore.enableVersioning();
    }
}

module.exports = Encore.getWebpackConfig();
