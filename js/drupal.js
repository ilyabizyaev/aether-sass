/**
 * A collection of hack for theming drupal
 */

import './../css/drupal.scss';

window.onload = () => {
    document.querySelectorAll('.navbar-nav').forEach(navbar => {
        for (let li of navbar.children) {
            li.classList.add('nav-item');
            li.querySelector('a').classList.add('nav-link');
        }
    });

    document.querySelectorAll('.nav.nav-tabs').forEach(navbar => {
        for (let li of navbar.children) {
            li.querySelector('a').classList.add('nav-link');
        }
    });

    document.querySelectorAll('.dropdown-menu').forEach(dropdown_menu => {
        for (let a of dropdown_menu.children) {
            a.classList.add('dropdown-item');
            a.classList.remove('nav-link');
        }
    });
}
